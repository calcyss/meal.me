class Restaurant < ActiveRecord::Base
  has_one :vote
  has_many :win_histories
  mount_uploader :attachment, AttachmentUploader
end
