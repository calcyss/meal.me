class VotingController < ApplicationController



  def start_vote
    minutes = params[:minutes].to_i
    state = State.all.first
    state.vote_running = true
    state.timer_sec = 0
    state.timer_min = minutes
    state.save
    render :nothing => true
  end

  def show_dashboard
    state = State.find(1)
    if user_signed_in?
      set_vars
    else
      redirect_to user_session_path, method: :post
    end
  end

  def switch_user_present
    id = params["id"].to_i
    usr = User.find id
    usr.is_present = !usr.is_present
    usr.save
    render :nothing => true
  end

  def vote_over
    set_vars
    render :template => "voting/vote_over/vote_over"
  end

  def vote_ended
    set_vars
    render :template => "voting/vote_ended/vote_ended"
  end

  def vote_running
    set_vars
    render :template => "voting/vote_running/vote_running"
  end

  def vote_for
    vote_id = params[:id].to_i
    vote = Vote.find(vote_id)
    vote.votes+=1
    vote.save
    vote_record_array = VotingRecord.where('user_id = ?',current_user.id)
    unless vote_record_array.empty?
      vote_record = vote_record_array.first
      old_vote = Vote.find(vote_record.vote_id)
      old_vote.votes-=1
      old_vote.save
      vote_record.vote_id = vote.id
      vote_record.save
    else
      vote_record = VotingRecord.new(:user_id => current_user.id, :vote_id => vote.id)
      vote_record.save
    end
    render :nothing => true
  end

  def new_vote
    v = Vote.new(restaurant_id: params[:restaurant_id].to_i, votes: 0)
    v.save
    render :nothing => true
  end
  def seen_end
    usr = User.find(current_user.id)
    usr.seen_end = true
    usr.save
    render :nothing => true
  end
  def get_votes
    votes = Vote.all
    render :json => votes.to_json(include: :restaurant)
  end

  def get_state
    state = State.find(1)
    render :json => state.to_json
  end
  def get_users
    users = User.all
    render :json => users
  end
  def get_current_user
    usr = User.find(current_user.id)
    if WinHistory.all.last.blank?
      usr.seen_end = true
      usr.save
    end
    render :json => usr
  end

  def winhistory_exists
    wh = WinHistory.all.last
    data = {}
    if wh.present?
      if wh.votes.present? && wh.restaurant.present? && wh.user.present?
        data["exists"] = true
      else
        data["exists"] = false
      end
    else
      data["exists"] = false
    end
    render :json => data
  end

  private
  def set_vars
    @votes = Vote.all
    @users = User.all
    @restaurants = Restaurant.all
    state = State.find(1)
    @timer = state.timer_min
    @vote_running = state.vote_running
    if WinHistory.all.last.blank?
      current_user.seen_end = true
      current_user.save
    end
    @seen_end = current_user.seen_end
    @vote_count = @votes.length
    @user_count = @users.length
  end

end
