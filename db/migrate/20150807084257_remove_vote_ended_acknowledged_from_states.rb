class RemoveVoteEndedAcknowledgedFromStates < ActiveRecord::Migration
  def change
    remove_column :states, :vote_ended_acknowledged
    remove_column :states, :vote_reset_acknowledged
  end
end
