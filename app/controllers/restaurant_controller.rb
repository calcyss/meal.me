class RestaurantController < ApplicationController

  protect_from_forgery except: :new_restaurant

  def new_restaurant
    rest = Restaurant.new(name: params["name"], nick: params["nick"], phone_number: params["phone"], address: params["address"], attachment: params["menu"])
    puts params["menu"]
    rest.save
    render :json => rest
  end
  def get_restaurants
    rests = Restaurant.all
    render :json => rests
  end
end
