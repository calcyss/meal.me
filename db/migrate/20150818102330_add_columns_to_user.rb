class AddColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :seen_end, :boolean, :default => false
    add_column :users, :is_present, :boolean, :default => false
  end
end
