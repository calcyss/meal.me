Rails.application.routes.draw do


  devise_for :users


  get '/get_votes' => 'voting#get_votes', :defaults => {:format => "json"}
  get '/get_restaurants' => 'restaurant#get_restaurants', :defaults => {:format => "json"}
  get '/get_state' => 'voting#get_state', :defaults => {:format => "json"}
  get '/get_users' => 'voting#get_users', :defaults => {:format => "json"}
  get '/get_current_user' => 'voting#get_current_user'
  get '/get_vote_over' => 'voting#vote_over'
  get '/get_vote_running' => 'voting#vote_running'
  get '/get_vote_ended' => 'voting#vote_ended'
  get '/winhistory_exists' => 'voting#winhistory_exists'
  post '/end_vote' => 'voting#end_vote', as: "end_vote"
  post '/start_vote' => 'voting#start_vote'
  post '/new_restaurant' => 'restaurant#new_restaurant'
  post '/new_vote' => 'voting#new_vote'
  post '/vote_for' => 'voting#vote_for'
  post '/seen_end' => 'voting#seen_end'
  post '/switch_user_present' => "voting#switch_user_present"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  root 'voting#show_dashboard'
  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
