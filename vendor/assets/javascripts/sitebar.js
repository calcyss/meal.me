/**
 * Created by daniel on 19.08.15.
 */
$(document).ready(function(){
	$("*[data-sidebar]").each(function(){
		$(this).on("click",function(){
			var sidebar = $(this).data("sidebar");
			$(sidebar).toggleClass("active");
		});
	});
	$("*[data-overlay]").each(function(){
		$(this).on("click",function(){
			var overlay= $(this).data("overlay");
			$(overlay).fadeToggle("fast");
		});
	});
	$("*[data-dropdown]").each(function(){
		$(this).on("click",function(){
			var dropdown = $(this).data(dropdown);
			$(dropdown).toggleClass("active");
		});
	});
});