class CreateWinHistory < ActiveRecord::Migration
  def change
    create_table :win_histories do |t|
      t.integer :restaurant_id
      t.integer :user_id
      t.integer :votes
    end
  end
end
