class AddSeenEndToUsers < ActiveRecord::Migration
  def change
    add_column :users, :seen_end, :boolean
  end
end