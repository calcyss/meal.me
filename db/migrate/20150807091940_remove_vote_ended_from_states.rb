class RemoveVoteEndedFromStates < ActiveRecord::Migration
  def change
    remove_column :states, :vote_ended
  end
end
