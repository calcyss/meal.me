scheduler = Rufus::Scheduler.new

def end_of_round
  users = User.all
  votes = Vote.all
  records = VotingRecord.all
  users.each do |u|
    u.seen_end = false
    u.save
  end
  winning_vote = votes.order('votes DESC').first
  last_wh = WinHistory.all.last
  if winning_vote.present?
    if last_wh.present?
    if last_wh.user_id > users.count
      next_user_id = last_wh.user_id - users.count
      user_absent = true
      while user_absent
        usr = User.find next_user_id
        if usr.is_present
          user_absent = false
        elsif !usr.is_present
          next_user_id += 1
        end
      end
      wh = WinHistory.new
      wh.restaurant = winning_vote.restaurant
      wh.user = User.find(next_user_id)
      wh.votes = winning_vote.votes
      wh.save
    else
      next_user_id = last_wh.user_id + 1
      if next_user_id > users.count
        next_user_id -= users.count
      end
      wh = WinHistory.new
      wh.restaurant = winning_vote.restaurant
      user_absent = true
      while user_absent
        usr = User.find next_user_id
        if usr.is_present
          user_absent = false
        elsif !usr.is_present
          next_user_id += 1
        end
      end
      wh.user = User.find(next_user_id)
      wh.votes = winning_vote.votes
      wh.save
    end
    else
      new_wh = WinHistory.new
      next_user_id = 1
      user_absent = true
      while user_absent
        usr = User.find next_user_id
        if usr.is_present
          user_absent = false
        elsif !usr.is_present
          next_user_id += 1
        end
      end
      new_wh.user = User.find next_user_id
      new_wh.restaurant = winning_vote.restaurant
      new_wh.votes = winning_vote.votes
      new_wh.save
    end
  end
  records.delete_all
  votes.delete_all
end

scheduler.every("10s") do
  state = State.all.first
  if state.vote_running
    if state.timer_min == 0
      state.vote_running = false
      end_of_round
    else
      if state.timer_sec == 60
        state.timer_sec = 0
        state.timer_min -= 1
      else
        state.timer_sec += 10
      end
    end
  end
  state.save
end